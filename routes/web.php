<?php

use App\Http\Controllers\Auth\LogoutContoller;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\Master\MasterController;
use App\Http\Controllers\Request\DepositRequestController;
use App\Http\Controllers\Request\UpgradeRequestController;
use App\Http\Controllers\Request\WithdrawRequestController;
use App\Http\Controllers\Resolution\ResolutionController;
use App\Http\Controllers\Setting\SettingController;
use App\Http\Controllers\Template\TemplateController;
use App\Http\Controllers\Transaction\TransactionController;
use App\Http\Controllers\User\UserController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('login', [AuthController::class, 'showLoginForm'])->name('login');
Route::post('login', [AuthController::class, 'login']);

Route::group(['middleware' => 'auth'], function () {

    Route::get('/', [TransactionController::class, 'index'])->name('index');

    Route::get('signout', [LogoutContoller::class, 'signout'])->name('signout');

    Route::group(['prefix' => 'transaction'], function () {
        Route::get('/', [TransactionController::class,'index'])->name('transaction');
        Route::get('/{id}', [TransactionController::class,'detail'])->name('transaction-detail');
        Route::post('/{id}/update', [TransactionController::class,'update'])->name('transaction-update');
    });

    Route::group(['prefix' => 'request'], function () {

        Route::group(['prefix' => 'upgrade'], function () {
            Route::get('/', [UpgradeRequestController::class,'index'])->name('upgrade-request');
            Route::get('/{id}', [UpgradeRequestController::class,'detail'])->name('upgrade-reques-detail');
            Route::post('/{id}/update', [UpgradeRequestController::class,'update'])->name('upgrade-request-update');
        });

        Route::group(['prefix' => 'deposit'], function () {
            Route::get('/', [DepositRequestController::class,'index'])->name('deposit-request');
            Route::get('/{id}', [DepositRequestController::class,'detail'])->name('deposit-request-detail');
            Route::post('/{id}/update', [DepositRequestController::class,'update'])->name('deposit-request-update');
        });

        Route::group(['prefix' => 'withdraw'], function () {
            Route::get('/', [WithdrawRequestController::class,'index'])->name('withdraw-request');
            Route::get('/{id}', [WithdrawRequestController::class,'detail'])->name('withdraw-request-detail');
            Route::post('/{id}/update', [WithdrawRequestController::class,'update'])->name('withdraw-request-update');
        });

    });

    Route::group(['prefix' => 'resolution'], function () {
        Route::get('/', [ResolutionController::class,'index'])->name('resolution');
        Route::get('/{id}', [UserController::class,'detail'])->name('resolution-detail');
    });

    Route::group(['prefix' => 'users'], function () {
        Route::get('/', [UserController::class,'index'])->name('users');
        Route::get('/{id}', [UserController::class,'detail'])->name('user-detail');
        Route::post('/{id}/update', [UserController::class,'update'])->name('user-update');
    });

    Route::group(['prefix' => 'template'], function () {
        Route::get('/', [TemplateController::class,'index'])->name('template');
        Route::get('/{id}', [TemplateController::class,'detail'])->name('template-detail');
        Route::post('/{id}/update', [TemplateController::class,'update'])->name('template-update');
    });

    Route::group(['prefix' => 'master'], function () {
        Route::get('/', [MasterController::class,'index'])->name('master');
        Route::get('/{id}', [MasterController::class,'detail'])->name('master-detail');
        Route::post('/{id}/update', [MasterController::class,'update'])->name('master-update');
    });

    Route::group(['prefix' => 'settings'], function () {
        Route::get('/', [SettingController::class,'index'])->name('settings');
        Route::get('/{id}', [SettingController::class,'detail'])->name('settings-detail');
        Route::post('/{id}/update', [SettingController::class,'update'])->name('settings-update');
    });
    
});

// locale Route
Route::get('lang/{locale}', [LanguageController::class, 'swap']);

Auth::routes();
