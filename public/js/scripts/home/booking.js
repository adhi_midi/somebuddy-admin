$inputParticipant = 1;
$inputHour = 1;
$price = 0;
$totalPrice = 0;
$currentAddress = "";
$currentLat = -6.175456395000281;
$currentLong = 106.82716352557946;
$startTime = 12;

function inputHour(){
    var $inputGroup = $('.input-group-hour');
    var $btnMinus = $inputGroup.find('.btn-minus');
    var $btnPlus = $inputGroup.find('.btn-plus');
    var $input = $inputGroup.find('.input-number');

    var $minVal = 1;
    var $maxVal = $inputGroup.attr('data-max-val');


    $btnMinus.on('click', function(e){
        e.preventDefault();
        if ($inputHour > $minVal){
            $inputHour -= 1;
            $input.val($inputHour).change();
        } else {
            console.log("min val");
        }
    });

    $btnPlus.on('click', function(e){
        e.preventDefault();
        if ($inputHour < $maxVal){
            $inputHour += 1;
            $input.val($inputHour).change();
        } else {
            console.log("max val");
        }
    });

    $input.change(function() {
        if ($inputHour == $maxVal){
            $btnPlus.removeClass('btn-outline-primary');
            $btnPlus.addClass('btn-outline-secondary');
            $btnPlus.prop('disabled', true);
        } else if ($inputHour == $minVal){
            $btnMinus.removeClass('btn-outline-primary');
            $btnMinus.addClass('btn-outline-secondary');
            $btnMinus.prop('disabled', true);
        } else {
            $btnPlus.removeClass('btn-outline-secondary');
            $btnPlus.addClass('btn-outline-primary');
            $btnPlus.prop('disabled', false);
            $btnMinus.removeClass('btn-outline-secondary');
            $btnMinus.addClass('btn-outline-primary');
            $btnMinus.prop('disabled', false);
        } 

        onValueChange();
    });
}

function inputParticipant(){
    var $inputGroup = $('.input-group-participant');
    var $btnMinus = $inputGroup.find('.btn-minus');
    var $btnPlus = $inputGroup.find('.btn-plus');
    var $input = $inputGroup.find('.input-number');

    var $minVal = 1;
    var $maxVal = $inputGroup.attr('data-max-val');


    $btnMinus.on('click', function(e){
        e.preventDefault();
        if ($inputParticipant > $minVal){
            $inputParticipant -= 1;
            $input.val($inputParticipant).change();
        } else {
            console.log("min val");
        }
    });

    $btnPlus.on('click', function(e){
        e.preventDefault();
        if ($inputParticipant < $maxVal){
            $inputParticipant += 1;
            $input.val($inputParticipant).change();
        } else {
            console.log("max val");
        }
    });

    $input.change(function() {
        if ($inputParticipant == $maxVal){
            $btnPlus.removeClass('btn-outline-primary');
            $btnPlus.addClass('btn-outline-secondary');
            $btnPlus.prop('disabled', true);
        } else if ($inputParticipant == $minVal){
            $btnMinus.removeClass('btn-outline-primary');
            $btnMinus.addClass('btn-outline-secondary');
            $btnMinus.prop('disabled', true);
        } else {
            $btnPlus.removeClass('btn-outline-secondary');
            $btnPlus.addClass('btn-outline-primary');
            $btnPlus.prop('disabled', false);
            $btnMinus.removeClass('btn-outline-secondary');
            $btnMinus.addClass('btn-outline-primary');
            $btnMinus.prop('disabled', false);
        } 

        onValueChange();
    });
}

const formatToCurrency = amount => {
  return amount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
};

function onValueChange(){
    var $totalParticipantText = $('.total-participant');
    var $totalHourText = $('.total-hour');
    var $totalPriceText = $('.total-price');

    $totalParticipantText.html($inputParticipant);
    $totalHourText.html($inputHour);
    $totalPrice = ($inputParticipant * $inputHour) * $price;
    $totalPriceText.html(formatToCurrency($totalPrice));

    var $inputStartTime = $('.input-start-time');
    var $inputEndTime = $('.input-end-time');

    $strStart = $inputStartTime.select2().val();
    $strEnd = $inputEndTime.select2().val();
    $start = $strStart.split(":");
    $end = parseInt($start[0]) + $inputHour;

    if ($end < 10){
        $endTime = "0" + $end.toString() + ":00";
    } else {
        $endTime = $end.toString() + ":00";
    }

    $inputEndTime.select2().val($endTime).trigger("change");

    console.log($endTime)

}

function postBooking(){

    var $obj = {};
    $obj["service_id"] = parseInt($('.service-profile').attr('data-service'));
    $obj["location"] = $('.input-destination').val();
    $obj["location_lat"] = $('.input-destination').attr('data-lat');
    $obj["location_long"] = $('.input-destination').attr('data-lng');
    $obj["participant"] = $inputParticipant;
    $obj["duration"] = $inputHour;
    $obj["start_time"] = $('.input-start-time').val();
    $obj["end_time"] = $('.input-end-time').val();
    $obj["date"] = $('.input-date').val();
    $obj["price"] = $price;
    var $json = JSON.stringify($obj);

    $.ajax({
        url: '/booking/store',
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 'application/json',
            Accept: 'application/json'
        },
        contentType: 'application/json',
        dataType: 'json',
        data: $json,
        success: function(response){
            window.location.replace("/checkout");
        },
        error: function(response) {
            toastr['warning'](response.responseJSON.message, {
                timeOut: 1000
            });
        }
    });
}


function initMap($lat, $lng) {

    var mapOptions = {
      zoom: 14,
      center: { lat: parseFloat($lat), lng:  parseFloat($lng)},
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById('map'),
        mapOptions);

    $('<div/>').addClass('centerMarker').appendTo(map.getDiv())
        .click(function(){
           var that=$(this);
           if(!that.data('win')){
            that.data('win',new google.maps.InfoWindow({content:'this is the center'}));
            that.data('win').bindTo('position',map,'center');
           }
           that.data('win').open(map);
    });


    google.maps.event.addListener(map, 'dragend', function(evt){
        $currentLat = map.getCenter().lat();
        $currentLong = map.getCenter().lng();
        loadMapLocation(map.getCenter().lat(), map.getCenter().lng());
    });
}

function loadMapLocation($lat, $long){

    const geocoder = new google.maps.Geocoder();
    const latlng = {
        lat: $lat,
        lng: $long,
    };

    $address = "";

    geocoder
        .geocode({ location: latlng })
        .then((response) => {
            if (response.results[0]) {
                $address=response.results[0].formatted_address;
            } else {
                toastr['warning']("No results found", {
                    timeOut: 1000
                });
            }
        })
        .catch((e) => {
            toastr['warning']("Geocoder failed due to: " + e, {
                timeOut: 1000
            });
        });
}

function saveLocation(){
    
}


$(function () {
    'use strict';

    var $btnCheckout = $('.btn-checkout');
    var $inputDestination = $('.input-destination');
    var $btnSave = $('.btn-save-location');

    $price = $('.service-profile').attr('data-price');

    $inputDestination.attr('data-lat', $currentLat);
    $inputDestination.attr('data-lng', $currentLong);

    var $inputStartTime = $('.input-start-time');
    var $inputEndTime = $('.input-end-time');

    $inputStartTime.select2().val("12:00").trigger("change");
    $inputEndTime.select2().val("13:00").trigger("change");

    inputHour();
    inputParticipant();
    onValueChange();

    $btnCheckout.on('click', function(e){
        e.preventDefault();
        postBooking();
    });

    $inputDestination.on('click', function(e){
        e.preventDefault();
        initMap($inputDestination.attr('data-lat'), $inputDestination.attr('data-lng'));
        $('#modal-destination').modal('toggle');
    });

    $btnSave.on('click', function(e){
        e.preventDefault();
        
        $inputDestination.attr('data-lat', $currentLat);
        $inputDestination.attr('data-lng', $currentLong);

        var $locationDetail = $('.input-location-detail').val();
        $inputDestination.val($locationDetail);

        $('#modal-destination').modal('toggle');
    });

    var $inputDate = $('.input-date');

    $inputDate.flatpickr({
        dateFormat: "d-m-Y",
        minDate: "today",
        defaultDate: String($inputDate.attr("data-date"))
    });

});