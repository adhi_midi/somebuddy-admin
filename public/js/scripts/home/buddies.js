function postWishlist($btn){

    $obj = {};
    $obj["id"] = $btn.attr('data-service');
    $json = JSON.stringify($obj);

    $.ajax({
        url: '/wishlist/add',
        type: 'POST',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
          'Content-Type': 'application/json',
          Accept: 'application/json'
        },
        contentType: 'application/json',
        dataType: 'json',
        data: $json,
        success: function(response){
            if ($btn.hasClass('active')){
                $btn.removeClass('active');
            } else {
                $btn.addClass('active');
            }
            toastr['success'](response.message, {
                timeOut: 1000
            });
            
        },
        error: function(response) {
          toastr['warning'](response.responseJSON.message, {
            timeOut: 1000
          });
        }
    });
}

$(function () {
    'use strict';

    $(document).on('click', '.btn-wishlist', function (e) {
        e.preventDefault();
        if ($(this).data('service')) {
            console.log('goes here')
            postWishlist($(this));
        } else {
            window.location.replace("/login");
        }
    });

    $(document).on('click', '.item-buddy', function (e) {
        e.preventDefault();
        var $data = $(this).data('service');
        window.location.replace("/buddies/" + $data);
    });

});