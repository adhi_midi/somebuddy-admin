function chatAction($opponentId){
  
  $obj = {};
  $obj["user_id_to"] = $opponentId;
  $json = JSON.stringify($obj);

  $.ajax({
    url: '/inbox/store',
    type: 'POST',
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      'Content-Type': 'application/json',
      Accept: 'application/json'
    },
    contentType: 'application/json',
    dataType: 'json',
    data: $json,
    success: function(response){
      window.location.replace("/inbox");
    },
    error: function(response) {
      toastr['warning'](response.responseJSON.message, {
        timeOut: 1000
      });
    }
  });
}

$(function () {
  'use strict';

  var $userRating = $('.user-rating'),
    $btnChat = $('.btn-chat');

  if ($userRating.length) {
      var rating = $userRating.attr('data-rating');
      $userRating.rateYo({
        rating: rating,
        starWidth: "25px"
      });
    }

  if ($btnChat.length){
    $btnChat.on('click', function(){
      chatAction($(this).attr('data-user'));
    });
  }

  
});