
function postCheckout(){

    var $id = $('.checkout-detail').attr('data-checkout');

    var $obj = {};
    $obj["payment"] = 1;
    var $json = JSON.stringify($obj);

    $.ajax({
        url: '/checkout/' + $id,
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 'application/json',
            Accept: 'application/json'
        },
        contentType: 'application/json',
        dataType: 'json',
        data: $json,
        success: function(response){
            window.location.replace("/");
        },
        error: function(response) {
            toastr['warning'](response.responseJSON.message, {
                timeOut: 1000
            });
        }
    });
}

function loadPaymentDetail($data){
    var $paymentTitle = $('.payment-title');
    var $paymentDesc= $('.payment-desc');
    var $imageBank= $('.image-bank .image');
    $paymentTitle.html($data.bank_account);
    $paymentDesc.html($data.description);
    $imageBank.attr('src', 'images/icon/' + $data.image);
}

function getPaymentDetail($id){
    $.ajax({
        url: '/checkout/payment/' + $id,
        type: 'GET',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 'application/json',
            Accept: 'application/json'
        },
        contentType: 'application/json',
        dataType: 'json',
        success: function(response){
            loadPaymentDetail(response.data);
        },
        error: function(response) {
            toastr['warning'](response.responseJSON.message, {
                timeOut: 1000
            });
        }
    });
}


$(function () {
    'use strict';
    var $btnCheckout = $('.btn-checkout');

    getPaymentDetail(1);

    $btnCheckout.on('click', function(e){
        e.preventDefault();
        postCheckout();
    });

    var $selectPayment = $('.select-payment');

    $selectPayment.on("select2:select", function (e) { 
        getPaymentDetail(this.value);
    });
});