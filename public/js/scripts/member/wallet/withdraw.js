function postWithdraw(){

  var $inputWithdraw = $('.input-withdraw').val();
  var $balance = $inputWithdraw.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '')
  
  var $bank = $('.input-bank');

  $obj = {};
  $obj["balance"] = parseInt($balance);
  $obj["bank"] = $bank.select2().val();
  $json = JSON.stringify($obj);

  $.ajax({
      url: '/withdraw/store',
      type: 'POST',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      contentType: 'application/json',
      dataType: 'json',
      data: $json,
      success: function(response){
        window.location.replace("/wallet");
      },
      error: function(response) {
        toastr['warning'](response.responseJSON.message, {
          timeOut: 1000
        });
      }
  });
}

function loadPaymentDetail($data){
  var $paymentTitle = $('.payment-title');
  var $paymentDesc= $('.payment-desc');
  var $imageBank= $('.image-bank .image');
  $paymentTitle.html($data.bank_account);
  $paymentDesc.html($data.description);
  $imageBank.attr('src', '/images/icon/' + $data.image);
}

function formatNumber(n) {
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")
}

function formatCurrency(input) {
  
  var input_val = input.val();
  if (input_val === "") { return; }
  
  var original_len = input_val.length;

  var caret_pos = input.prop("selectionStart");
    
  input_val = formatNumber(input_val);
  input_val = input_val;

  input.val(input_val);

  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}

$(function () {

  var $inputWithdraw = $('.input-withdraw');

  $inputWithdraw.on({
      keyup: function() {
        formatCurrency($(this));
      }
  });

  var $btnSend = $('.btn-send');

  $btnSend.on('click', function(e){
    e.preventDefault();

    
    postWithdraw();
  });

});