function postBankAccount(){

  var $formAdd = $('.form-add');
  var $bankType = $formAdd.find('.input-bank');
  var $bankAccount = $formAdd.find('.input-bank-account');

  $obj = {};
  $obj["bank_id"] = $bankType.select2().val();
  $obj["bank_account"] = $bankAccount.select2().val();
  $json = JSON.stringify($obj);

  $.ajax({
      url: '/bank/store',
      type: 'POST',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      contentType: 'application/json',
      dataType: 'json',
      data: $json,
      success: function(response){
        location.reload();
      },
      error: function(response) {
        toastr['warning'](response.responseJSON.message, {
          timeOut: 1000
        });
      }
  });
}

function deleteBankAccount($id){
  $.ajax({
    url: '/bank/' + $id + "/delete",
    type: 'POST',
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      'Content-Type': 'application/json',
      Accept: 'application/json'
    },
    contentType: 'application/json',
    dataType: 'json',
    success: function(response){
      toastr['success'](response.message, {
        timeOut: 1000
      });
    },
    error: function(response) {
      toastr['warning'](response.responseJSON.message, {
        timeOut: 1000
      });
      console.log(toString(response.message));
    }
  });
}

$(function () {

  var $btnSave = $('.btn-save');

  $btnSave.on('click', function(e){
    e.preventDefault();
    postBankAccount();
  });

  $(document).on('click', '.btn-delete-bank', function (e) {
    e.preventDefault();

    $(this).closest('.list-group-item').remove();

    var $id = $(this).attr('data-bank');
    if ($id != 0) {
      deleteBankAccount($id)
    } else {
      toastr['success']('Bank Account deleted', {
        timeOut: 1000
      });
    }

  });

});