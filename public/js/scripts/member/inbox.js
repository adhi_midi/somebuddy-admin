$opponentId = 0;
$userId = 0;

function showChatWindow($id){
  var $chatWindow = $('.chat-app-window .active-chat');
  $chatWindow.attr('data-chat', $id);
  $chatWindow.removeClass('hidden');
  $chatWindow.find('.chats').children().remove();
}

function setHeaderChat($response){
  var $opponentImage = $('.chat-header .opponent_avatar img'),
    $opponentName = $('.chat-header .opponent_name');

  $opponentImage.attr('src', 'images/profile/thumb/' + $response.opponent['image_profile']);
  $opponentName.html($response.opponent['name']);
}

function setChats($response){
  $userId = $response.user['id'];
  $opponentId = $response.opponent['id'];
  $('.dropdown-item.profile').attr('href', "buddies/" + $opponentId)

  var $userChats = $('.user-chats .chats');

  var $chats = $response.data;

  for (let i = 0; i < $chats.length; ++i) {

    $pastDate = '';
    
    if (i != 0){
      $pastDate = moment($chats[i-1].datetime).format('DD-MM-YYYY');
    }

    $currentDate = moment($chats[i].datetime).format('DD-MM-YYYY');
    $today = moment();

    $date = '';

    if (moment($chats[i].datetime).diff($today, 'days') == 0){
      $date = 'Today';
    } else if (moment($chats[i].datetime).diff($today, 'days') == -1){
      $date = 'Yesterday';
    } else {
      $date = moment($chats[i].datetime).format('DD MMMM YYYY');
    }

    $time = moment($chats[i].datetime).format('HH:mm');

    if ($currentDate != $pastDate){
      $userChats.append('<div class="divider"><div class="divider-text">' + $date + '</div></div>');
    }

    if ($chats[i].user_id == $opponentId){
      $userChats.append('<div class="chat chat-left"><div class="chat-body"> <div class="chat-content">'
      + '<p>' + $chats[i].message + '</p> '
      + '<p class="font-small-2 text-black-50 text-right">' + $time + '</p> '
      +'</div> </div> </div>')
    } else {
      $userChats.append('<div class="chat"><div class="chat-body"> <div class="chat-content">'
      + '<p>' + $chats[i].message + '</p> '
      + '<p class="font-small-2 text-black-50 text-left">' + $time + '</p> '
      +'</div> </div> </div>')
    }
  }

  $('.user-chats').scrollTop($('.user-chats > .chats').height());

}

function getChatDetail($id){
  $.ajax({
    url: '/inbox/' + $id,
    type: 'GET',
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      'Content-Type': 'application/json',
      Accept: 'application/json'
    },
    contentType: 'application/json',
    dataType: 'json',
    success: function(response){
      showChatWindow($id);
      setHeaderChat(response);
      setChats(response);
    },
    error: function(response) {
      toastr['warning'](response.responseJSON.message, {
        timeOut: 1000
      });
    }
  });
}

function loadPostedChat($message){
  var $userChats = $('.user-chats .chats');

  $time = moment().format('HH:mm');

  $userChats.append('<div class="chat"><div class="chat-body"> <div class="chat-content">'
      + '<p>' + $message + '</p> '
      + '<p class="font-small-2 text-black-50 text-left">' + $time + '</p> '
      +'</div> </div> </div>')

  $('.message').val('');
  $('.user-chats').scrollTop($('.user-chats > .chats').height());
}

function postChat($message){
  var $chatWindow = $('.chat-app-window .active-chat');
  $chatId = $chatWindow.attr('data-chat');

  $obj = {};
  $obj["id"] = $chatId;
  $obj["message"] = $message;
  $obj["user_id"] = $userId;
  $json = JSON.stringify($obj);

  $.ajax({
    url: '/inbox/' + $chatId + '/update',
    type: 'POST',
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      'Content-Type': 'application/json',
      Accept: 'application/json'
    },
    contentType: 'application/json',
    dataType: 'json',
    data: $json,
    success: function(response){
      loadPostedChat($message);
    },
    error: function(response) {
      toastr['warning'](response.responseJSON.message, {
        timeOut: 1000
      });
    }
  });
}

$(function () {
  var appContent = $('.app-content'),
    $lastChatGroup = $('.data-type').attr('data-chat-init');

  if ($lastChatGroup.length){
    getChatDetail($lastChatGroup);
  }
  
  appContent.addClass('chat-application');

  $('.chat-users-list li').on('click', function(){
    getChatDetail($(this).attr('data-inbox'));
  });

});


$(window).on('resize', function () {
  if ($(window).width() > 992) {
    if ($('.chat-application .body-content-overlay').hasClass('show')) {
      $('.app-content .sidebar-left').removeClass('show');
      $('.chat-application .body-content-overlay').removeClass('show');
    }
  }

  if ($(window).width() < 991) {
    if (
      !$('.chat-application .chat-profile-sidebar').hasClass('show') ||
      !$('.chat-application .sidebar-content').hasClass('show')
    ) {
      $('.sidebar-content').removeClass('show');
      $('.body-content-overlay').removeClass('show');
    }
  }
});

function enterChat(source) {
  var message = $('.message').val();
  postChat(message);
}
