$inputParticipant = 1;

function inputParticipant(){
  var $inputGroup = $('.input-group-participant');
  var $btnMinus = $inputGroup.find('.btn-minus');
  var $btnPlus = $inputGroup.find('.btn-plus');
  var $input = $inputGroup.find('.input-number');

  var $minVal = 1;

  $btnMinus.on('click', function(e){
      e.preventDefault();
      if ($inputParticipant > $minVal){
          $inputParticipant -= 1;
          $input.val($inputParticipant).change();
      } else {
          console.log("min val");
      }
  });

  $btnPlus.on('click', function(e){
      e.preventDefault();
      $inputParticipant += 1;
      $input.val($inputParticipant).change();
  });

  $input.on('change', function(e){
      if ($inputParticipant == $minVal){
          $btnMinus.removeClass('btn-outline-primary');
          $btnMinus.addClass('btn-outline-secondary');
          $btnMinus.prop('disabled', true);
      } else {
          $btnPlus.removeClass('btn-outline-secondary');
          $btnPlus.addClass('btn-outline-primary');
          $btnPlus.prop('disabled', false);
          $btnMinus.removeClass('btn-outline-secondary');
          $btnMinus.addClass('btn-outline-primary');
          $btnMinus.prop('disabled', false);
      } 
  });
}

function formatNumber(n) {
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")
}

function formatCurrency(input) {
  
  var input_val = input.val();
  if (input_val === "") { return; }
  
  var original_len = input_val.length;

  var caret_pos = input.prop("selectionStart");
    
  input_val = formatNumber(input_val);
  input_val = input_val;

  input.val(input_val);

  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}

function addSchedule() {
  var $inputSchedule = $('.input-schedule');
  var $scheduleList = $('.schedule-list');

  if ($scheduleList.children().length < 5){
    $inputSchedule.find('.select2.form-control').select2("destroy").end();
    $inputSchedule.children().clone().appendTo('.schedule-list');

    $scheduleList.find('.select2.form-control').each(function () {
      var $this = $(this);
      $this.wrap('<div class="position-relative"></div>');
      $this.select2({
        dropdownAutoWidth: true,
        dropdownParent: $this.parent(),
        width: '100%',
        containerCssClass: 'select-md'
      });
    });

    $inputSchedule.find('.select2.form-control').select2();
  }
}

function createService(){
  var $scheduleList = $('.schedule-list');
  var $activity = $('.input-activity').select2().val();

  var $currency = $('.input-price').val();
  var $price = $currency.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '');

  var $participant = $('.input-participant').val();
  var $description = $('.input-description').val();

  var $totalSchedule = $scheduleList.children();

  var $obj = {};
  $obj["activity"] = $activity;
  $obj["price"] = parseInt($price);
  $obj["participant"] = $participant;
  $obj["description"] = $description;

  var $schedules = [];

  for (let i = 0; i < $totalSchedule.length; i++) {

    var $schedule = {};
    var $days = [];

    $scheduleList.children().eq(i).find("input:checkbox[name=days]").each(function(){
      if($(this).is(':checked')){
        $days.push($(this).val());
      }
    });

    $schedule["days"] = $days;
    $schedule["start_time"] = $scheduleList.children().eq(i).find(".input-start-time").select2().val();
    $schedule["end_time"] = $scheduleList.children().eq(i).find(".input-end-time").select2().val();

    $schedules.push($schedule);
    
  }

  $obj["schedules"] = $schedules;
  var $json = JSON.stringify($obj);

  $.ajax({
      url: '/settings/services/add',
      type: 'POST',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      contentType: 'application/json',
      dataType: 'json',
      data: $json,
      success: function(response){
        window.location.replace("/settings");
      },
      error: function(response) {
        toastr['warning'](response.responseJSON.message, {
          timeOut: 1000
        });
      }
  });
}

$(function () {
  'use strict';

  var 
    $btnAddSchedule = $('.btn-add-schedule'),
    $btnCreateService = $('.btn-create-service');
  
  $btnAddSchedule.on('click', function(e){
    e.preventDefault();
    addSchedule();
  })
  
  $(document).on('click', '.btn-delete-schedule', function (e) {
    e.preventDefault();
    $(this).closest('.card').remove();
  });

  $btnCreateService.on('click', function(e){
    e.preventDefault();
    createService();
  });

  var $inputPrice = $('.input-price');

  $inputPrice.on({
      keyup: function() {
        formatCurrency($(this));
      }
  });

  inputParticipant();
});