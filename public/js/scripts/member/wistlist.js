function postWishlist($btn){

    $obj = {};
    $obj["id"] = $btn.attr('data-service');
    $json = JSON.stringify($obj);

    $.ajax({
        url: '/wishlist/add',
        type: 'POST',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
          'Content-Type': 'application/json',
          Accept: 'application/json'
        },
        contentType: 'application/json',
        dataType: 'json',
        data: $json,
        success: function(response){
            $btn.closest('.col-md-4').remove();
        },
        error: function(response) {
          toastr['warning'](response.responseJSON.message, {
            timeOut: 1000
          });
        }
    });
}

$(function () {
    'use strict';

    $(document).on('click', '.btn-wishlist', function (e) {
        e.preventDefault();
        postWishlist($(this));
    });
});