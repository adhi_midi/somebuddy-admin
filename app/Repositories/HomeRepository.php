<?php

namespace App\Repositories;

use App\Models\Notification\Notification;
use App\Repositories\BaseRepository;
use App\Models\User;
use App\Models\UserService;
use App\Models\UserWishlist;
use App\Models\Master\City;
use App\Models\Master\Price;
use App\Models\Master\Activity;
use App\Models\Front\HeroBanner;
use App\Models\Front\HomeActivities;
use App\Models\Front\AboutUs;
use App\Models\Front\AboutUsItem;
use App\Models\Front\HomeBuddies;
use App\Models\Front\Banner;
use App\Models\Front\CallToAction;
use App\Models\Front\SocialMedia;
use App\Models\Front\HomeTestimonial;

class HomeRepository extends BaseRepository
{
    protected $heroBannerModel, $masterActivityModel;

    public function __construct(
        HeroBanner $heroBannerModel,
        HomeActivities $activitiesModel,
        AboutUs $aboutUsModel,
        AboutUsItem $aboutUsItemModel,
        HomeBuddies $buddiesModel,
        Banner $bannerModel,
        HomeTestimonial $testimonialModel,
        CallToAction $ctaModel,
        SocialMedia $socmedModel,
        Activity $masterActivityModel   
        )
    {
        $this->heroBannerModel = $heroBannerModel;
        $this->activitiesModel = $activitiesModel;
        $this->aboutUsModel = $aboutUsModel;
        $this->aboutUsItemModel = $aboutUsItemModel;
        $this->buddiesModel = $buddiesModel;
        $this->bannerModel = $bannerModel;
        $this->testimonialModel = $testimonialModel;
        $this->ctaModel = $ctaModel;
        $this->socmedModel = $socmedModel;
    }

    public function getHeroBanner()
    {
        return $this->heroBannerModel->findOrFail(1);
    }

    public function getActivities()
    {
        $query = $this->activitiesModel
            ->join(config('database.connections.mysql_master.database') . '.activities as t1', 't1.id', '=', 'home_activities.activity_id')
            ->orderBy('id', 'ASC')
            ->get(['home_activities.*', 't1.name', 't1.name_en', 't1.image', 't1.description', 't1.description_en']);

        return $query;
    }

    public function getAboutUs()
    {
        return $this->aboutUsModel->get()->last();
    }

    public function getAboutUsItem()
    {
        return $this->aboutUsItemModel->get()->all();
    }

    public function getHomeBuddies()
    {
        $query = $this->buddiesModel
            ->join(config('database.connections.mysql.database') . '.user_service as t1', 't1.id', '=', 'home_buddies.service_id')
            ->join(config('database.connections.mysql.database') . '.users as t2', 't2.id', '=', 'home_buddies.user_id')
            ->join(config('database.connections.mysql_master.database') . '.cities as t3', 't3.city_id', '=', 't2.location')
            ->join(config('database.connections.mysql_master.database') . '.activities as t4', 't4.id', '=', 't1.activity_id')
            ->orderBy('home_buddies.id', 'ASC')
            ->get(['t1.id', 't1.rating','t2.id as user_id','t2.name as user_name', 't2.image_profile', 't3.city_name', 't4.name as service_name', 't1.price']);

        return $query;
    }

    public function getBanner()
    {
        return $this->bannerModel->get()->all();
    }

    public function getTestimonial()
    {
        return $this->testimonialModel->where('status', 1)->orderBy('id', 'desc')->get()->all();
    }

    public function getCallToAction()
    {
        return $this->ctaModel->get()->last();
    }

    public function getSocmed()
    {
        return $this->socmedModel->get()->all();
    }

    public function getAllActivities()
    {
        return $this->masterActivityModel->get()->all();
    }
}