<?php

namespace App\Repositories;

use App\Models\Notification\Notification;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use App\Models\User;
use App\Models\UserImageGallery;
use App\Models\UserService;
use App\Models\UserServiceSchedule;
use App\Models\UserReview;
use App\Models\UserWishlist;
use App\Models\Master\City;
use App\Models\Master\Price;
use App\Models\Master\Activity;
use App\Models\Front\HeroBanner;

class BuddyRepository extends BaseRepository
{
    protected $heroBannerModel, $serviceModel;

    public function __construct(
        HeroBanner $heroBannerModel,
        UserService $serviceModel
        )
    {
        $this->heroBannerModel = $heroBannerModel;
        $this->serviceModel = $serviceModel;
    }

    public function getHeroBanner()
    {
        return $this->heroBannerModel->findOrFail(2);
    }

    public function getServices($request){

        // if (!empty($request->date)){
        //     $dates = explode(" to ", strval($request->date));
        //     $startDate = $dates[0];
        //     $endDate = $dates[1];
        // }

        // $cityId = 1;

        // if (!empty($request->city)){
        //     $cityId = $request->city;
        // }

        if (empty($request->activity) && empty($request->city)){
            $query = $this->serviceModel
                ->join('users as t1', 't1.id', '=', 'user_service.user_id')
                ->join(config('database.connections.mysql_master.database') . '.cities as t2', 't2.city_id', '=', 't1.location')
                ->join(config('database.connections.mysql_master.database') . '.activities as t3', 't3.id', '=', 'user_service.activity_id')
                ->get(['user_service.id', 'user_service.rating','t1.id as user_id','t1.name as user_name', 't1.image_profile', 't2.city_name', 't3.name as service_name', 'user_service.price']);
        }else if(!empty($request->activity) && empty($request->city)){
            $query = $this->serviceModel
                ->where('t3.id', $request->activity)
                ->join('users as t1', 't1.id', '=', 'user_service.user_id')
                ->join(config('database.connections.mysql_master.database') . '.cities as t2', 't2.city_id', '=', 't1.location')
                ->join(config('database.connections.mysql_master.database') . '.activities as t3', 't3.id', '=', 'user_service.activity_id')
                ->get(['user_service.id', 'user_service.rating','t1.id as user_id','t1.name as user_name', 't1.image_profile', 't2.city_name', 't3.name as service_name', 'user_service.price']);
        }else {
            $query = $this->serviceModel
                ->where('t3.id', $request->activity)
                ->where('t2.city_id', $request->city)
                ->join('users as t1', 't1.id', '=', 'user_service.user_id')
                ->join(config('database.connections.mysql_master.database') . '.cities as t2', 't2.city_id', '=', 't1.location')
                ->join(config('database.connections.mysql_master.database') . '.activities as t3', 't3.id', '=', 'user_service.activity_id')
                ->get(['user_service.id', 'user_service.rating','t1.id as user_id','t1.name as user_name', 't1.image_profile', 't2.city_name', 't3.name as service_name', 'user_service.price']);
        }


        return $query;
    }

    public function getProfileById($id){
        

        return ;
    }

    public function getServicesByUserId($id){
        
        $query = UserService::where('user_id', $id)
        ->join(config('database.connections.mysql_master.database') . '.activities as t1', 't1.id', '=', 'user_service.activity_id')
        ->orderBy('updated_at', 'desc')
        ->get(['user_service.*', 't1.name AS service_name', 'user_service.price AS price_name']);

        return $query;
    }

    public function getReviewByUserId($id){
        
        $query = UserReview::where('buddy_id', $id)
        ->join('users as t1', 't1.id', '=', 'user_review.user_id')
        ->join('user_service as t2', 't2.id', '=', 'user_review.service_id')
        ->join(config('database.connections.mysql_master.database') . '.activities as t3', 't3.id', '=', 't2.activity_id')
        ->get(['t1.id AS user_id', 't1.name AS user_name', 't1.image_profile AS user_image' , 't1.birthdate AS user_birthdate', 'user_review.description', 'user_review.rating', 't3.name AS service_name']);

        return $query;
    }

    public function getGalleryByUserId($id){
        $query = UserImageGallery::where('user_id', $id)->get()->all();
        return $query;
    }
}