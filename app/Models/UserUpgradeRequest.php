<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserUpgradeRequest extends Model
{
    use HasFactory;
    protected $table = 'user_upgrade_request';

    protected $fillable = [
        'user_id',
        'status',
    ];
}
