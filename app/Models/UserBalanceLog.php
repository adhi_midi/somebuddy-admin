<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBalanceLog extends Model
{
    use HasFactory;

    protected $table = 'user_balance_log';

    protected $fillable = [
        "user_id", 
        "transaction_id",
        "log_type",
        "status"
    ];
}
