<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceTransaction extends Model
{
    use HasFactory;

    protected $table = 'service_transaction';

    protected $fillable = [
        "service_id", 
        "user_id", 
        "invoice_no", 
        "location", 
        "participant", 
        "start_time", 
        "end_time",
        "duration",
        "date",
        "total_price",
        "service_price",
        "fee_price",
        "payment_type",
        "booked_at",
        "confirm_at",
        "confirm_payment_at",
        "finish_at",
        "status",
        "buddy_id", 
    ];
}
