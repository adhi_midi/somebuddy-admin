<?php

namespace App\Models\Front;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomeBuddies extends Model
{
    use HasFactory;
    protected $connection ='mysql_front';
    protected $table = 'home_buddies';
}
