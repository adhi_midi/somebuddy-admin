<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserImageGallery extends Model
{
    use HasFactory;

    protected $table = 'user_image_gallery';

    protected $fillable = [
        "user_id", 
        "image"
    ];
}
