<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserImageIdentity extends Model
{
    use HasFactory;

    protected $table = 'user_image_identity';

    protected $fillable = [
        "user_id", 
        "image_identity",
        "image_selfie"
    ];
}
