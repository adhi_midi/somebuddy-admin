<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserDepositRequest extends Model
{
    use HasFactory;

    protected $table = 'user_deposit_request';

    protected $fillable = [
        "user_id", 
        "bank",
        "deposit",
        "status"
    ];
}
