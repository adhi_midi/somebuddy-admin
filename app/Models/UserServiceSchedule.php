<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserServiceSchedule extends Model
{
    use HasFactory;

    protected $table = 'user_service_schedule';

    protected $fillable = [
        'service_id',
        'days',
        'start_time',
        'end_time',
        'status',
    ];
}
