<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserInboxGroup extends Model
{
    use HasFactory;

    protected $table = 'user_inbox_group';

    protected $fillable = [
        "user_id", 
        "user_id_to"
    ];
}
