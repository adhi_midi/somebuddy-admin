<?php

namespace App\Services;

use App\Services\BaseService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;
use App\Models\User;
use App\Models\UserWishlist;
use App\Repositories\HomeRepository;

class HomeService extends BaseService
{
    protected $repo;

    public function __construct(
        HomeRepository $repo
        )
    {
        parent::__construct();
        $this->repo = $repo;
    }

    public function getHeroBanner(){

        $data = [];
        
        $heroBanner = $this->repo->getHeroBanner();
        $data['image'] = $heroBanner->image;

        if (App::isLocale('id')) {
            $data['title'] = $heroBanner->title;
        } else{
            $data['title'] = $heroBanner->title_en;
        }

        return $data;
    }
    
    public function getActivities(){
    
        $datas = array();

        $activities = $this->repo->getActivities();

        foreach($activities as $item){

            $data = [];
            $data['id'] = $item->activity_id;
            $data['image'] = $item->image;

            if (App::isLocale('id')) {
                $data['title'] = $item->name;
                $data['desc'] = $item->description;
            } else{
                $data['title'] = $item->activity_name_en;
                $data['desc'] = $item->description_en;
            }

            array_push($datas, $data);
        }

        return $datas;
    }
    
    public function getAboutUs(){
    
        $datas = [];

        $aboutUs = $this->repo->getAboutUs();

        if (App::isLocale('id')) {
            $datas['title'] = $aboutUs->title;
            $datas['desc'] = $aboutUs->description;
        } else{
            $datas['title'] = $aboutUs->title_en;
            $datas['desc'] = $aboutUs->description_en;
        }

        $aboutUsItems = $this->repo->getAboutUsItem();
        $aboutItemArr = array();

        foreach ($aboutUsItems as $data){
            $aboutItem = [];
            $aboutItem['image'] = $data->image;

            if (App::isLocale('id')) {
            $aboutItem['title'] = $data->title;
            } else{
            $aboutItem['title'] = $data->title_en;
            }

            array_push($aboutItemArr, $aboutItem);
        }

        $datas['about_us_item'] = $aboutItemArr; 

        return $datas;
    }
      
    public function getBuddies(){

        $datas = array();
        
        if (!Auth::guest()){
            $userId = Auth::user()->id;
        } 

        $homeBuddies = $this->repo->getHomeBuddies();

        foreach ($homeBuddies as $item){ 

            $data = [];
            $data['user_id'] = $item->user_id;
            $data['user_name'] = $item->user_name;
            $data['user_image'] = $item->image_profile;
            $data['service_id'] = $item->id;
            $data['service_name'] = $item->service_name;
            $data['price'] = $item->price;
            $data['location'] = $item->city_name;
            $data['rating'] = $item->rating;

            if (!Auth::guest()){

                $wishlist = UserWishlist::where('user_id', $userId)->where('service_id',$item->id)->get()->first();
                
                if (empty($wishlist) || $wishlist->status == 0){
                    $data['wishlist'] = false;
                } else {
                    $data['wishlist'] = true;
                }

            }

            array_push($datas, $data);
        }

        return $datas;
    }
    
    public function getBanner(){
    
        $datas = array();
        
        $banners = $this->repo->getBanner();

        foreach ($banners as $data){

            $banner = [];
            $banner['id'] = $data->id;
            $banner['type'] = $data->type;
            $banner['image'] = $data->image;
            $banner['link'] = $data->link;

            if (App::isLocale('id')) {
                $banner['title'] = $data->title;
                $banner['desc'] = $data->description;
            } else{
                $banner['title'] = $data->title_en;
                $banner['desc'] = $data->description_en;
            }

            array_push($datas, $banner);
        }

        return $datas;
    }
    
    public function getTestimonial(){
    
        $datas = array();

        $testimonials = $this->repo->getTestimonial();

        foreach ($testimonials as $key => $data){
            if ($key < 3){
            $testimonial = [];
            $testimonial['image'] = $data->image;
            $testimonial['age'] = $data->age;
            $testimonial['name'] = $data->name;

            if (App::isLocale('id')) {
                $testimonial['desc'] = $data->description;
            } else{
                $testimonial['desc'] = $data->description_en;
            }

            array_push($datas, $testimonial);
            }
        }

        return $datas;
    }
    
    public function getCallToAction(){
    
        $datas = [];

        $callToAction = $this->repo->getCallToAction();

        $datas['image'] = $callToAction->image;
        $datas['link'] = $callToAction->link;

        if (App::isLocale('id')) {
            $datas['title'] = $callToAction->title;
        } else{
            $datas['title'] = $callToAction->title_en;
        }

        return $datas;
    }
    
    public function getSocailMedia(){
    
        $socialMedia = $this->repo->getSocmed();

        return $socialMedia;
    }

    public function getAllActivities(){
        return $this->repo->getAllActivities();
    }

}