<?php

namespace App\Services;

use App\Services\BaseService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;
use Carbon\Carbon;
use App\Models\User;
use App\Models\UserImageGallery;
use App\Models\UserService;
use App\Models\UserServiceSchedule;
use App\Models\UserReview;
use App\Models\UserWishlist;
use App\Models\Master\Gender;
use App\Models\Master\Language;
use App\Models\Master\Nationality;
use App\Models\Master\City;
use App\Models\Master\Activity;
use App\Models\Master\Interest;
use App\Models\Master\Price;
use App\Repositories\BuddyRepository;

class BuddyService extends BaseService
{
    protected $repo;

    public function __construct(
        BuddyRepository $repo
        )
    {
        parent::__construct();
        $this->repo = $repo;
    }

    public function getHeroBanner(){

        $data = [];
        
        $heroBanner = $this->repo->getHeroBanner();
        $data['image'] = $heroBanner->image;

        if (App::isLocale('id')) {
            $data['title'] = $heroBanner->title;
        } else{
            $data['title'] = $heroBanner->title_en;
        }

        return $data;
    }

    public function getServices($request){

        $datas = array();
        
        if (!Auth::guest()){
          $userId = Auth::user()->id;
        } 
    
        $services = $this->repo->getServices($request);
    
        foreach ($services as $item){ 
    
          $data = [];
          $data['user_id'] = $item->user_id;
          $data['user_name'] = $item->user_name;
          $data['user_image'] = $item->image_profile;
          $data['service_id'] = $item->id;
          $data['service_name'] = $item->service_name;
          $data['price'] = $item->price;
          $data['location'] = $item->city_name;
          $data['rating'] = $item->rating;
    
          if (!Auth::guest()){
    
            $wishlist = UserWishlist::where('user_id', $userId)->where('service_id',$item->id)->get()->first();
          
            if (empty($wishlist) || $wishlist->status == 0){
              $data['wishlist'] = false;
            } else {
              $data['wishlist'] = true;
            }
    
          }
    
          array_push($datas, $data);
        }
    
        return $datas;
    }


  public function getUserDetail($id){

    $user = User::findOrFail($id);

    $languages = Language::get()->all();
    $activities = Activity::get()->all();
    $interests = Interest::get()->all();

    $profile = [];
    $profile['id'] = $user->id;
    $profile['name'] = $user->name;
    $profile['image'] = $user->image_profile;
    $profile['user_type'] = $user->user_type;

    $birthdate = Carbon::parse($user->birthdate);
    $now = Carbon::now();

    $profile['age'] = $birthdate->diffInYears($now);
    
    $profile['phone'] = $user->phone_number;
    $profile['about'] = $user->about;

    if (App::isLocale('id')) {
      $profile['gender'] = Gender::where('id', $user->gender)->get()->first()->name;
    } else{
      $profile['gender'] = Gender::where('id', $user->gender)->get()->first()->name_en;
    }

    if (!empty($user->nationality)){
      $profile['nationality'] = Nationality::where('id', $user->nationality)->get()->first()->name;
    } else {
      $profile['nationality'] = '';
    }

    if (!empty($user->nationality)){
      $profile['location'] = City::where('city_id', $user->location)->get()->first()->city_name;
    } else {
      $profile['location'] = '';
    }

    if (!empty($user->language)){
      $language = array();
      foreach ($languages as $data){
        if (in_array($data->id, array_map('intval', json_decode($user->language)))){
          array_push($language, $data->name);
        }
      }
      $profile['language'] = $language;
    } else {
      $profile['language'] = [];
    }

    if (!empty($user->activity)){
      $activity = array();
      foreach ($activities as $data){
        if (in_array($data->id, array_map('intval', json_decode($user->activity)))){
          array_push($activity, $data->name);
        }
      }
      $profile['activity'] = $activity;
    } else {
      $profile['activity'] = [];
    }

    if (!empty($user->interest)){
      $interest = array();
      foreach ($interests as $data){
        if (in_array($data->id, array_map('intval', json_decode($user->interest)))){
          array_push($interest, $data->name);
        }
      }
      $profile['interest'] = $interest;
    } else {
      $profile['interest'] = [];
    }

    $profile['review'] = $this->getTotalReview($this->getReviews($id));
    
    return $profile;
  }

  public function getUserServices($id){
  
    $datas = array();

    $services = $this->repo->getServicesByUserId($id);

    foreach ($services as $item){ 

      $data = [
        'id'  => $item->id,
        'name'  => $item->service_name,
        'price'  => number_format($item->price_name, 0, ',', '.'),
        'description'  => $item->description,
        'user_id'  => $item->user_id,
      ];

      array_push($datas, $data);
      
    }

    return $datas;

  }

  public function getTotalReview($reviews){

    $datas = [];

    $totalReview = count($reviews);
    $datas['reviews'] = $totalReview;

    $totalRating = 0;

    foreach( $reviews as $review){
      $totalRating += $review['rating'];
    }

    if ($totalReview != 0){
      $calculatedRating = $totalRating / $totalReview;
      $datas['rating'] = number_format((float)$calculatedRating, 1, '.', '');
    } else {
      $datas['rating'] = 0;
    }
     
    return $datas;
  }

  public function getReviews($id){

    $datas = array();

    $reviews = $this->repo->getReviewByUserId($id);

    foreach ($reviews as $item){ 

      $data = [];
      $data['service'] = $item->service_name;
      $data['rating'] = $item->rating;
      $data['desc'] = $item->description;
      $data['name'] = $item->user_name;
      $data['image'] = $item->user_image;

      $birthdate = Carbon::parse($item->user_birthdate);
      $now = Carbon::now();

      $data['age'] = $birthdate->diffInYears($now);

      array_push($datas, $data);
    }

    return $datas;
  }

  public function getGalleries($id){
    return $this->repo->getGalleryByUserId($id);
  }

}