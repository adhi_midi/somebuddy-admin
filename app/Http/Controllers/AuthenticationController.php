<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticationController extends Controller
{
  public function login()
  {
    $pageConfigs = ['blankPage' => true];

    return view('/content/authentication/auth-login', ['pageConfigs' => $pageConfigs]);
  }
  
  public function register()
  {
    $pageConfigs = ['blankPage' => true];

    return view('/content/authentication/auth-register', ['pageConfigs' => $pageConfigs]);
  }
  
  public function forgot_password()
  {
    $pageConfigs = ['blankPage' => true];

    return view('/content/authentication/auth-forgot-password', ['pageConfigs' => $pageConfigs]);
  }

  public function reset_password()
  {
    $pageConfigs = ['blankPage' => true];

    return view('/content/authentication/auth-reset-password', ['pageConfigs' => $pageConfigs]);
  }

  public function signout()
  {
      Auth::logout(); 
      return redirect()->route('login');
  }
}
