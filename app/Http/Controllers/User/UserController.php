<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
  public function index(){
    return view('/content/user/index');
  }

  public function detail($id){
    return view('/content/user/detail');
  }

}
