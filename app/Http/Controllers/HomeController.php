<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\HomeService;
use App\Models\Master\Activity;

class HomeController extends Controller
{

  protected $service;
  
  public function __construct(HomeService $service)
  {
      $this->service = $service;
  }

  public function index(){
    
    $pageConfigs = ['showMenu' => false];

    $heroBanner = $this->service->getHeroBanner();
    $activities = $this->service->getActivities();
    $aboutUs = $this->service->getAboutUs();
    $buddies = $this->service->getBuddies();
    $banners = $this->service->getBanner();
    $callToAction = $this->service->getCallToAction();
    $socialMedia = $this->service->getSocailMedia();
    $testimonials = $this->service->getTestimonial();

    $masterActivities = Activity::get()->all();;

    return view('/content/home/index', ['pageConfigs'=>$pageConfigs, 'masterActivities'=>$masterActivities, 'buddies'=>$buddies, 'heroBanner'=>$heroBanner, 'activities'=>$activities, 'aboutUs'=>$aboutUs, 'banners'=>$banners, 'callToAction'=>$callToAction, 'socialMedia'=>$socialMedia, 'testimonials'=>$testimonials]);
  }

  
}
