<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LogoutContoller extends Controller
{
    public function signout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
