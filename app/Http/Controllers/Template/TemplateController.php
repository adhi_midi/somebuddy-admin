<?php

namespace App\Http\Controllers\Template;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TemplateController extends Controller
{
  public function index(){
    return view('/content/template/index');
  }

  public function detail($id){
    return view('/content/template/detail');
  }

}
