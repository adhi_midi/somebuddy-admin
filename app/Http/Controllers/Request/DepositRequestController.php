<?php

namespace App\Http\Controllers\Request;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DepositRequestController extends Controller
{
  public function index(){
    return view('/content/request/deposit/index');
  }

  public function detail($id){
    return view('/content/request/deposit/detail');
  }

}
