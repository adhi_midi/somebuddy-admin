<?php

namespace App\Http\Controllers\Request;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UpgradeRequestController extends Controller
{
  public function index(){
    return view('/content/request/upgrade/index');
  }

  public function detail($id){
    return view('/content/request/upgrade/detail');
  }

}
