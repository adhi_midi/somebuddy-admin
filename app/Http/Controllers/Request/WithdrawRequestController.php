<?php

namespace App\Http\Controllers\Request;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WithdrawRequestController extends Controller
{
  public function index(){
    return view('/content/request/withdraw/index');
  }

  public function detail($id){
    return view('/content/request/withdraw/detail');
  }

}
