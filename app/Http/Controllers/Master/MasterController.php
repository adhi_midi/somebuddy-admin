<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MasterController extends Controller
{
  public function index(){
    return view('/content/master/index');
  }

  public function detail($id){
    return view('/content/master/detail');
  }

}
