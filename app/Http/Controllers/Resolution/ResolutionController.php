<?php

namespace App\Http\Controllers\Resolution;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResolutionController extends Controller
{
  public function index(){
    return view('/content/resolution/index');
  }

  public function detail($id){
    return view('/content/resolution/detail');
  }

}
