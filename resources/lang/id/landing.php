<?php

return [ 
  "hero_home" => "Kami Membantu Anda Menemukan Pelatih yang Tepat untuk Aktivitas Anda",
  "hero_line_1" => "Kami Membantu Anda",
  "hero_line_2" => "Menemukan Pelatih yang Tepat untuk Aktivitas Anda",
  "deal_of_the_week" => "Kesepakatan Minggu Ini",
  "our_buddies" => "Buddy Kita",
  "best_recommendation" => "Rekomendasi Terbaik",
  "why_choose_us" => "Mengapa Memilih Kami",
  "about_us" => "Tentang Kami",
  "about_us_title" => "Kami Membuat Semua Prosesnya Mudah",
  "about_us_desc" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus elementum, lacus vitae tristique convallis, leo felis dapibus lectus, in egestas elit dui eget quam",
  "what_they_say" => "Apa yang Mereka Katakan Tentang Somebuddy",
  "call_to_action" => "Dengan menjadi partner Somebuddy dapatkan penghasilan tambahan dengan cara yang mudah",
  "learn_more" => "Pelajari Lebih Lanjut",
  "connect_with_us" => "Terhubung dengan Kami",
  "activity_more" => "Temukan Lebih Banyak",
];
