{{-- Vendor Scripts --}}
<script src="{{ asset(mix('vendors/js/vendors.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/ui/prism.min.js')) }}"></script>
<script src="{{ asset(('vendors/js/moment/moment.js')) }}"></script>
<script src="{{ asset(('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(('vendors/js/extensions/dropzone.min.js')) }}"></script>
<script src="{{ asset(('vendors/js/fslightbox/fslightbox.js')) }}"></script>
<script src="{{ asset(('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="{{ asset(('vendors/js/extensions/toastr.min.js')) }}"></script>
<script src="{{ asset(('vendors/js/slick/slick.min.js')) }}"></script>
<script src="{{ asset(('js/scripts/components/components-modals.js')) }}"></script>
@yield('vendor-script')
{{-- Theme Scripts --}}
<script src="{{ asset(mix('js/core/app-menu.js')) }}"></script>
<script src="{{ asset(mix('js/core/app.js')) }}"></script>
{{-- page script --}}
@yield('page-script')
{{-- page script --}}
