@extends('layouts/contentLayoutMaster')

@section('title', 'Member Area - Request Withdraw')
@section('content')
<section>
  <div class="data-type" data-user-type="{{ Auth::user()->user_type }}"></div>
</section>
<section>
  <h2 class="page-title font-bolder text-primary mb-2">{{ __('locale.withdraw') }}</h2>
</section>

<section>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
        </div>
      </div>
    </div>
  </div>
</section>

@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(('js/scripts/page/request-withdraw.js')) }}"></script>
@endsection
