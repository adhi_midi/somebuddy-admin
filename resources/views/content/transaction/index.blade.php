@extends('layouts/contentLayoutMaster')

@section('title', 'Member Area - Transaction')
@section('content')
<section>
  <div class="data-type" data-user-type="{{ Auth::user()->user_type }}"></div>
</section>
<section>
  <h2 class="page-title font-bolder text-primary mb-2">{{ __('locale.transaction') }}</h2>
</section>

<section>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
        </div>
      </div>
    </div>
  </div>
</section>

@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(('js/scripts/page/transaction.js')) }}"></script>
@endsection
